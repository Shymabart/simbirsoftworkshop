package pageObject;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

public class WishlistPage {
    private WebDriver driver;
    public WishlistPage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
        this.driver = driver;
    }
    /**
     * Кнопка "Купить"
     */
    @FindBy(xpath = "//button[@data-role='buy-wishlist-button']")
    private WebElement buyButton;

    /**
     * Кнопка "Перейти в каталог"
     */
    @FindBy(xpath = "//a[@class='button-ui button-ui_brand profile-wishlist__empty-content-btn']")
    private WebElement catalogButton;

    /**
     * Список кнопок "Добавить в избранное"(максимум 18)
     */
    @FindBy(xpath = "//button[@class='button-ui button-ui_white button-ui_icon wishlist-btn button-ui_done']")
    private List<WebElement> wishlistList;

    @Step("Присутствует кнопка 'Купить'")
    public boolean buyButtonisDisplayed(){
        return buyButton.isDisplayed();
    }

    @Step("Присутствует кнопка 'В каталог")
    public boolean catalogButtonisDisplayed(){
        return catalogButton.isDisplayed();
    }

    @Step("Убрать товар из избранного")
    public void wishListClick(int productNumber){
        wishlistList.get(productNumber).click();
    }
}