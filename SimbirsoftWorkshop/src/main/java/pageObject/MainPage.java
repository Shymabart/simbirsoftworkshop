package pageObject;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import java.util.List;


public class MainPage {
    private WebDriver driver;

    public MainPage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
        this.driver = driver;
    }
    /**
     * Кнопка "Корзина"
     */
    @FindBy(xpath = "//span[@class='cart-link__lbl']")
    private WebElement basketButton;

    /**
     * Кнопка "Избранное"
     */
    @FindBy(xpath = "//span[@class='wishlist-link__lbl']")
    private WebElement wishlistButton;

    /**
     * Счет в  "Избранное"
     */
    @FindBy(xpath = "//span[@class='wishlist-link__badge']")
    private WebElement countWishlist;

    /**
     * Счет в "Корзине"
     */
    @FindBy(xpath = "//span[@class='cart-link__badge']")
    private WebElement countBasket;

    /**
     * Кнопка "Сравнить"
     */
    @FindBy(xpath = "//a[@href='/compare/']")
    private WebElement compareButton;

    /**
     * Лого в хедере
     */
    @FindBy(xpath = "//*[@id=\"header-logo\"]")
    private WebElement logoButton;

    /**
     * Поиск по сайту инпут
     */
    @FindBy(xpath = "/html/body/header/nav/div/div[1]/form/div/input")
    private WebElement searchInput;

    /**
     * Поиск по сайту кнопка
     */
    @FindBy(xpath = "/html/body/header/nav/div/div[1]/form/div/div[2]/span[2]")
    private WebElement searchButton;

    /**
     * Кнопка "Каталог"
     */
    @FindBy(xpath = "//span[@class='catalog-spoiler ']")
    private WebElement catalogButton;

    /**
     * Выпадашка"Каталога"
     */
    @FindBy(xpath = "//a[@class='ui-link menu-desktop__root-title']")
    private List<WebElement> catalogList;

    /**
     * Список категорий во вклдке "Бытовая техника"
     */
    @FindBy(xpath = "//a[@class='subcategory__item ui-link ui-link_blue']")
    private List<WebElement> techList;

    @Step("Ввести email на главной и нажать 'Далее'")
    public void searchText(String text){
        try {
            searchInput.click();
        }
        catch(org.openqa.selenium.StaleElementReferenceException ex)
        {
            searchInput.click();
        }
        searchInput.sendKeys(text);
        searchButton.click();
    }

    @Step("Нажать на кнопку 'Сравнить'")
    public void compareClick(){
        try {
            compareButton.click();
        }
        catch(org.openqa.selenium.StaleElementReferenceException ex)
        {
            compareButton.click();
        }
    }

    @Step("Нажать на кнопку 'Избранное'")
    public void openWishList(){
        wishlistButton.click();
    }

    @Step("Нажать на кнопку 'Корзина'")
    public void openBasket(){
        basketButton.click();
    }

    @Step("Нажать на Лого")
    public void logoClick(){
        logoButton.click();
    }

    @Step("Выбрать элемент из 'выпадашки'")
    public void getCatalog(int n){
        catalogButton.click();
        catalogList.get(n).click();
    }

    @Step("Проверить кол-во элементов из категории 'Бытовая техника'")
    public int getTechListSize(){
        return techList.size();
    }

    @Step("Получить счетчик в 'Избранном'")
    public String getCountWishlist(){
        return countWishlist.getText();
    }

    @Step("Получить счетчик в 'Корзине'")
    public String getCountBasket(){
        return countBasket.getText();
    }
}