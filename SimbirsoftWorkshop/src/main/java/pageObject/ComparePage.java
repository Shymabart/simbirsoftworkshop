package pageObject;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ComparePage {
    private WebDriver driver;
    public ComparePage(WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
        this.driver = driver;
    }
    /**
     * Кнопка "На главную"
     */
    @FindBy(xpath = "//div[@class='base-ui-button base-ui-button_white']")
    private WebElement compareButton;

    /**
     * Список сравнения пуст
     */
    @FindBy(xpath = "//h2[@class='empty-page__content-title']")
    private WebElement emptyNotice;

    /**
     * Кнопка "В каталог"
     */
    @FindBy(xpath = "//div[@class='base-ui-button base-ui-button_brand']")
    private WebElement catalogButton;

    @Step("Видимость кнопки 'На главную' на странице")
    public boolean isCompareButtonActiv(){
        return compareButton.isDisplayed();
    }

    @Step("Видимость кнопки 'В каталог' на странице")
    public boolean catalogButtonActiv(){
        return catalogButton.isDisplayed();
    }

    @Step("Получить текст из нотиса")
    public String getEmptyNotice(){
        return emptyNotice.getText();
    }
}