package pageObject;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StartPage {
    private WebDriver driver;
    public StartPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    /**
     * Кнопка войти
     */
    @FindBy(xpath = "//button[@data-role='login-button']")
    private WebElement enterButton;

    /**
     * Кнопка войти c паролем в попапе
     */
    @FindBy(xpath = "//div[@class='block-other-login-methods__password-caption']")
    private WebElement enterWithPassword;

    /**
     * Поле ввода email/телефона
     */
    @FindBy(xpath = "//input[@autocomplete='username']")
    private WebElement idInput;

    /**
     * Поле ввода пароля
     */
    @FindBy(xpath = "//input[@autocomplete='current-password']")
    private WebElement passwordInput;

    /**
     * Кнопка "Войти"/Авторизация
     */
    @FindBy(xpath = "//div[@class='base-ui-button base-ui-button_brand base-ui-button_big-flexible-width']")
    private WebElement authButton;

    /**
     * Нотис "Не все поля заполнены"
     */
    @FindBy(xpath = "//div[@class='error-message-block form-entry-with-password__error']")
    private WebElement notAllFieldsAreFilled;

    @Step("Авторизация пользователя")
    public void authorizationUser(String id, String password){
        enterButton.click();
        enterWithPassword.click();
        idInput.sendKeys(id);
        passwordInput.sendKeys(password);
        authButton.click();
    }

    @Step("Получить текст нотиса")
    public String getTextNotAllFieldsAreFilled(){return notAllFieldsAreFilled.getText(); }
}