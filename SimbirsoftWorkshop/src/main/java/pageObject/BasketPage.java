package pageObject;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class BasketPage {
    private WebDriver driver;

    public BasketPage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
        this.driver = driver;
    }
    /**
     * Кнопка "Удалить"
     */
    @FindBy(xpath = "//button[@class='menu-control-button remove-button']")
    private WebElement restoreButton;

    /**
     * Кнопка "Вернуть удаленный товар"
     */
    @FindBy(xpath = "//a[@class='empty-message-restore-btn ui-link_pseudolink']")
    private WebElement backProductButton;

    @Step("Нажaть кнопку 'Удалить'")
    public  void clickRestoreButton(){
        restoreButton.click();
    }

    @Step("Получить текст с кнопки 'Вернуть удаленный товар'")
    public String getTextBackProduct(){
       return backProductButton.getText();
    }
}