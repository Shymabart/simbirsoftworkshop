package pageObject;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ProductPage {
    private WebDriver driver;

    public ProductPage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
        this.driver = driver;
    }
    /**
     * Список кнопок "Добавить в избранное"(максимум 18)
     */
    @FindBy(xpath = "/html/body/div[1]/div/div[2]/div[2]/div[3]/div/div[1]/div[1]/div[4]/button[1]")
    private WebElement wishList;

    /**
     * Список кнопок "Купить"(максимум 18)
     */
    @FindBy(xpath = "//*[@id=\"search-results\"]/div[3]/div/div[1]/div[1]/div[4]/button[2]")
    private WebElement buyList;

    @Step("Добавить 'Избранное'")
    public void clickCatalog(){
        wishList.click();
        wishList.click();
    }

    @Step("Выбрать элемент из 'Выпадашки'")
    public void clickBuyElement(){
        buyList.click();
    }
}