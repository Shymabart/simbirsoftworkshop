import io.qameta.allure.*;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageObject.*;
import properties.ConfProperties;
import java.util.concurrent.TimeUnit;

public class RegistrationPageTest {
    public static StartPage startPage;
    public static WebDriver driver;

    @BeforeClass
    public static void setup() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        //определение пути до драйвера и его настройка
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        //создание экземпляра драйвера
        driver = new ChromeDriver(options);
        startPage = new StartPage(driver);
        //окно разворачивается на полный экран
        driver.manage().window().maximize();
        //задержка на выполнение теста = 10 сек.
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS).setScriptTimeout(10,TimeUnit.SECONDS);
        //получение ссылки на страницу входа из файла настроек
        driver.get(ConfProperties.getProperty("loginPage"));
    }

    @Epic(value = "Авторизация")
    @Feature(value = "Не успешная авторизация")
    @Story(value = "Поля не заполнены")
    @Test
    @Severity(value = SeverityLevel.CRITICAL)
    public void emptyAuthorization(){
        String notice = "Не все поля заполнены";
        startPage.authorizationUser("","");
        Assert.assertEquals("Нотис не соответствует", startPage.getTextNotAllFieldsAreFilled(), notice);
    }

    @Epic(value = "Авторизация")
    @Feature(value = "Не успешная авторизация")
    @Story(value = "Не верный ввод данных")
    @Test
    @Severity(value = SeverityLevel.CRITICAL)
    public void errorAuthorization(){
        String notice = "E-mail/ телефон или пароль указаны неверно.";
        startPage.authorizationUser("vuiv@gmai.com","12345tyu");
        Assert.assertEquals("Нотис не соответствует", startPage.getTextNotAllFieldsAreFilled(), notice);
    }

    @After
    public void closeBrowser() {
        driver.navigate().to("https://www.dns-shop.ru/logout"); }

    @AfterClass
    public static void tearDown() {
        driver.quit(); }
}
