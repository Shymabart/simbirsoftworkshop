import io.qameta.allure.*;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageObject.*;
import properties.ConfProperties;

import java.util.concurrent.TimeUnit;

public class MainPageTest {
    public static StartPage startPage;
    public static MainPage mainPage;
    public static ComparePage comparePage;
    public static ProductPage productPage;
    public static WishlistPage wishlistPage;
    public static BasketPage basketPage;
    public static WebDriver driver;

    @BeforeClass
    public static void setup() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        //определение пути до драйвера и его настройка
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        //создание экземпляра драйвера
        driver = new ChromeDriver(options);
        startPage = new StartPage(driver);
        mainPage = new MainPage(driver);
        comparePage = new ComparePage(driver);
        productPage = new ProductPage(driver);
        wishlistPage = new WishlistPage(driver);
        basketPage = new BasketPage(driver);
        //окно разворачивается на полный экран
        driver.manage().window().maximize();
        //задержка на выполнение теста = 10 сек.
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS).setScriptTimeout(10,TimeUnit.SECONDS);
        //получение ссылки на страницу входа из файла настроек
        driver.get(ConfProperties.getProperty("loginPage"));
    }
    @Epic(value = "Главная страница")
    @Feature(value = "Добавление в Избранное")
    @Story(value = "Добавление и удаление товара в избранное")
    @Test
    @Severity(value = SeverityLevel.MINOR)
    public void addWishlist() {
        startPage.authorizationUser("Irovanie@gmail.com","qwerty123");
        mainPage.searchText("iphone");
        productPage.clickCatalog();
        int wishlistCount = 1;
        int parseCount = Integer.parseInt (mainPage.getCountWishlist());
        Assert.assertEquals("Счетчик не равен 1",parseCount,wishlistCount);
        mainPage.openWishList();
        Assert.assertTrue("Кнопка отсутствует", wishlistPage.buyButtonisDisplayed());
        wishlistPage.wishListClick(0);
        Assert.assertTrue("Кнопка отсутствует", wishlistPage.catalogButtonisDisplayed());
    }
    @Epic(value = "Главная страница")
    @Feature(value = "Добавление в корзину")
    @Story(value = "Добавление и удаление товара в корзину")
    @Test
    @Severity(value = SeverityLevel.MINOR)
    public void addBasket() {
        startPage.authorizationUser("Irovanie@gmail.com","qwerty123");
        mainPage.searchText("iphone");
        productPage.clickBuyElement();
        int count = 1;
        int parseCount = Integer.parseInt (mainPage.getCountBasket());
        Assert.assertEquals("Счетчик не равен 1",parseCount,count);
        mainPage.openBasket();
        basketPage.clickRestoreButton();
        String parseNotice = basketPage.getTextBackProduct();
        String notice = "Вернуть удалённый товар";
        Assert.assertEquals("Текст не соответствует",parseNotice,notice);
    }
    @Epic(value = "Главная страница")
    @Feature(value = "Добавление в 'Сравнить'")
    @Story(value = "Пустой список для сравнения")
    @Test
    @Severity(value = SeverityLevel.MINOR)
    public void compareListIsEmpty() {
        String notice = "Список сравнения пуст";
        startPage.authorizationUser("Irovanie@gmail.com","qwerty123");
        mainPage.compareClick();
        Assert.assertEquals("Нотис не соответствует", comparePage.getEmptyNotice(), notice);
        Assert.assertTrue("Кнопки нет на странице", comparePage.isCompareButtonActiv());
        Assert.assertTrue("Кнопки нет на странице", comparePage.catalogButtonActiv());
    }

    @Epic(value = "Главная страница")
    @Feature(value = "Добавление в 'Сравнить'")
    @Story(value = "Переход в Категории из разделс Сравнить")
    @Test
    @Severity(value = SeverityLevel.MINOR)
    public void compareAppliacesList() {
        int blockSize = 3;
        startPage.authorizationUser("Irovanie@gmail.com","qwerty123");
        mainPage.compareClick();
        mainPage.getCatalog(0);
        Assert.assertEquals("Кол-во блоков не равно 3", mainPage.getTechListSize(), blockSize);
    }

    @After
    public void closeBrowser() {
        driver.navigate().to("https://www.dns-shop.ru/logout"); }

    @AfterClass
    public static void tearDown() {
        driver.quit(); }
}
